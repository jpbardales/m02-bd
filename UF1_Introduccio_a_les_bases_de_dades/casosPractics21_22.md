## Gabinet d'advocats

Es vol dissenyar una base de dades relacional per a emmagatzemar informació
sobre els assumptes que porta un gabinet d'advocats.
- Cada assumpte té un número d'expedient que l'identifica i correspon a un sol
client.
- De l'assumpte s'ha d'emmagatzemar la data d'inici, data d'arxiu (finalització)
, el seu estat (en tràmit, arxivat, etc), així com les dades personals del
client al qual pertany (DNI, nom, adreça, telèfon).
- Alguns assumptes són portats per un o diversos procuradors i viceversa, dels
quals ens interessa també les dades personals (DNI, nom, adreça, telèfon).


## Zoos  
Es vol dissenyar una base de dades relacional per emmagatzemar informació relativa als zoos existents en el món, així com les espècies i animals que aquests alberguen.
- De cada zoo es vol emmagatzemar el seu codi, nom, la ciutat i país on es troba, mida (m2) i pressupost anual.
- Cada zoo codifica els seus animals amb un codi propi, de manera que entre zoos es pot donar el cas que es repeteixi.
- De cada espècie animal s'emmagatzema un codi d'espècie, el nom vulgar, el nom científic, família a la qual pertany i si es troba en perill d'extinció.
- A més, s'ha de guardar informació sobre cada animal que els zoos tenen, com el seu número d'identificació, espècie, sexe, any de naixement, país d'origen i continent.

Establiu els atributs que considereu oportuns i d'ells, trieu l'`indentificador` adient per a cada entitat.

## Gestió de projectes

Es desitja dissenyar una base de dades que reculli la informació sobre la gestió de projectes que es porta a terme a l'empresa i la participació dels empleats.

* De cada empleat necessitarem saber el seu DNI, nom, adreça, telèfon i projecte en el qual participa. Cada projecte s'identificarà per un codi que serà únic, tindrà un títol, una durada estimada, una durada real, un pressupost.

* De cada projecte es vol saber el director del projecte, de manera que la mateixa persona pot dirigir diferents projectes. Modificar l'esquema anterior per recollir els nous requeriments d'usuari.
* Es desitja saber de cada empleat qui és el seu cap en cas que el tingui. Modificar l'esquema anterior per recollir els nous requeriments d'usuari.
* Cada empleat està assignat a un únic departament que s'encarregarà d'unes tasques determinades dins de l'empresa. Del departament de vol conèixer el nom. Modificar l'esquema anterior per recollir els nous requeriments d'usuari.

Nota: Es demana esquema E/R amb les seves entitats, atributs i interrelacions ben descrites, així com les claus primàries ben definides.

## ETT

Desitgem informatitzar la informació que manipula una ETT sobre empreses que ofereixen llocs de treball i persones que busquen feina. Les empreses ofereixen llocs de treball, informant de la professió sol·licitada, el lloc de treball destinat i les condicions exigides per a aquest lloc. De les persones que busquen feina tenim el seu DNI, nom, estudis i professió desitjada. Ens interessa saber quines persones poden optar a un lloc de treball, és a dir, poden participar en el procés de selecció. La persona interessada en un lloc es podrà apuntar per fer una entrevista per a aquest lloc. Per a cada lloc de treball es poden inscriure totes les persones interessades. En alguns casos es formalitzaran contractes entre les empreses i les persones, i emmagatzemarem la data de signatura, durada i sou del contracte. De
l'empreses tenim les dades de CIF, nom i sector. A més, es distingiran pimes i multinacionals: de les primeres emmagatzemarem la ciutat en la qual s'ubica i de les segones el nombre de països en els quals té representació.

Nota: establiu les claus primàries que considereu oportunes.

## Metro

Ens demanen  dissenyar  l'esquema E/R d'una bases de dades per gestionar les línies de metro de la ciutat de Barcelona. Cada línia està composada d'un conjunt d'estacions en un ordre determinat (tenint estació d'inici i de final). Cada línia té un identificador composat per una L i un número (L1, L2, L3, ..). Es vol emmagatzemar el color, hora de sortida del primer tren de la línia i del darrer. De cada línia volem saber l'estació d'inici i l'estació final de línia.


Les estacions tenen un codi numèric que les distingeix de les altres. També tenen un nom diferent per cadascuna. S'han d'emmagatzemar els horaris d'apertura i tancament de cada estació.


Una estació pot pertanyer a més d'una línia (p.e. Passeig de Gràcia pertany a la L4 i L2).

Cada estació té un o més accesos des de l'exterior i pot tindre ascensor, escales mecàniques o escales normals. Un accés se numera en funció de la línia, de manera que entre les diferents línies un número d'accés es por repetir. Els accesos estan numerats de manera correlativa (1, 2, 3, ..) per a cada estació, de manera que entre estacions la numeració coincidirà. Per cada accés s'emmagatzema el carrer i número de portal més proper on està situat.

Cada línia té assignats un conjunt de trens. No pot succeir que un tren estigui assignat a més d'una línia, però si pot passar que un tren no estigui associada a cap perquè està en reparació. Cada tren té un codi (que és únic), data de compra, model i número  de vagons.

En algunes estacions hi ha cotxeres per aparcar els trens quan no estan de servei. CAda tren té assignada una cotxera. Cada cotxera es distingeix pel mateix codi que l'estació. També es vol saber el  úmero de màquines i el de vagons que es poden allotjar.

Mitjançant un sistema GPS es coneix la posició de cada tren en servei.  Es necessita emmagatzemar l'estació a la que està arribant cada tren i els minuts estimats de l'arribada. Quan el tren està a l'anden els minuts valdran 0. En la mateixa estació poden haver-hi varis trens, un per cada sentit de cada línia en l'estació.

#  publicitat (6_publicitat.md)

Es proposa dissenyar una BD per millorar el control dels "spots" (anuncis) per a la televisió, i de tot el seu entorn, com són els canals de TV, franges horàries, agències de publicitat, tipus de productes que s'anuncien etc.

  Al país hi ha diversos ens televisius. Es vol reflectir que uns són de titularitat pública, per exemple CCMA (Corporació Catalana de Mitjans Audiovisuals) i RTVE (Ràdio-Televisió Espanyola) i altres són de titularitat privada com Atresmedia. Aquests ens televisius disposen d'un o més canals d'emissió. Per exemple, CCMA disposa de TV3, El 33, Canal Super3, etc. RTVE disposa de La 1, La 2, 24 hores, Tododeporte, etc. Atresmedia disposa de La Sexta, Antena 3, etc. Cada canal s'identificarà sempre per un codi, tindrà un nom i una descripció.

  Tots els "espots" suposarem que s'identifiquen per un codi assignat per una suposada "Oficina de Mitjans de Comunicació". Ha de considerar l'existència de "spots" equivalents, els quals podrien ser aquells que tenen les mateixes imatges però tenen diferent idioma.

  Cada "espot" fa referència a un (el normal) o més (excepcionalment) tipus de productes (pensar en rentadores i detergents), i es vol tenir constància d'aquestes referències. Fins i tot s'ha de pensar que hi ha tipificats certs tipus de productes, independentment de si hi ha o no, en aquest moment, espots que facin referència a ells. Els "spots" són sempre propietat d'una única firma comercial, que és la que els paga, fins i tot en el cas d'anunciar més d'un tipus de producte.
  Els "spots" són filmats, normalment, per una agència de publicitat encara que, en alguns casos, és la mateixa firma comercial que, amb mitjans propis, els produeix sense intervenció de cap agència publicitària.

  Tal com s'ha indicat a l'principi, interessa també conèixer l'emissió dels "spots" en els diversos canals televisius. A efectes d'audiència, les 24 hores del dia no s'entenen com a tals hores, sinó com a "franges horàries" (com poden ser: matí, migdia, tarda, nit, ...) perfectament diferenciades. És més, el preu vigent (únic que interessa) de l'emissió en cada canal, depèn de la seva franja horària. Per simplificar el cas, farem la hipòtesi que el preu no depèn del dia de la setmana. Es vol tenir constància de l'nombre de vegades que cada "espot" s'ha emès en els diferents canals, la seva data d'emissió i la seva corresponent franja horària.

  Finalment, hi ha un aspecte legal molt important a considerar. Hi ha alguns tipus de productes (realment molt pocs) que no estan permesos anunciar en certes franges horàries. L'incompliment serà penalitzat amb multes de gravetat qualificada d'1 a 3. Interessarà tenir constància de les prohibicions legals anteriors.

Opcional:

De les agències de publicitat interessa conèixer el nom de el director artístic. De les firmes comercials interessa conèixer el nom de el cap de màrqueting. Però d'unes i altres són tipus d'empreses, de les quals interessa conèixer el CIF (codi d'identificació fiscal), nom, adreça i telèfon.


Les agències de publicitat disposen de directors cinematogràfics per filmar els "espots". Interessa conèixer l'historial de la contractació per part de les diferents agències d'aquests directors. Un director treballa, en un moment donat, per a una sola agència. Les agències volen conèixer de cada "spot" que director l'ha dirigit. Dels "espots" produïts directament per les firmes comercials, no interessa ni es vol tenir constància del seu director.


## CAS PRÀCTIC: EL CINÈFIL (7_cinefil.md)


Un cinèfil aficionat a la informàtica vol crear una Base de Dades que reculli informació diversa sobre el món cinematogràfic, des dels orígens del cinema fins a avui mateix, amb el contingut que es descriu a continuació.
Lògicament, vol tenir classificades moltes pel.lícules, que vindran identificades per un codi. També vol de cadascuna el nom, l’any de l’estrena, el pressupost, el director, etc. A més, de cada pel.lícula vol conéixer també quins actors van intervenir, així com el paper que hi representàven (actor principal, secundari, etc.) i el possible premi que va rebre per la seva interpretació.
Les pel.lícules són d’un tema determinat. Es ben sabut que hi ha actors especialitzats en un tema, encara que un actor és capaç d’interpretar varis temes amb diferent “habilitat”.
Com que el nostre cinèfil és una mica curiós, vol emmagatzemar també dades personals dels actors, que ha anat recollint al llegir revistes del món artístic. Per exemple, quins actors són en certa manera substitutius d’altres, amb un grau de possible substitució que pot anar de 1 a 10. També quins actors són “incompatibles”, o sigui, que mai han treballat ni treballaran junts amb una mateixa pel.lícula o escena.
Els actors estan contractats, en un moment donat per una companyia, però poden canviar si tenen una oferta millor. També poden retornar a una companyia en la que ja hi  havien treballat. Les companyies produeixen pel.lícules, però cap pel.lícula és coproduïda per dues o més companyies.
Com que el nostre amic fa molt de turisme, vol saber, per a cada ciutat, quines companyies hi tenen representació i a quina adreça. Evidentment, les companyies solen tenir representació a quasi totes les ciutats importants. Al mateix temps, vol també informació de quines pel.lícules s’estan rodant a cada ciutat i en quin moment, tenint en compte que una pel.lícula es pot rodar a vàries ciutats i també a una mateixa ciutat en diferents fases del seu rodatge.


Qüestions


1. Proposar un esquema entitat-relació per a la B.D. en qüestió. Identificar clarament entitats, atributs i relacions.
2. Proposar un esquema lògi relacional amb el seu diagrama referencial.
