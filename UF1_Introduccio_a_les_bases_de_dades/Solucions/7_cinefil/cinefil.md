# 1. CINÈFIL
Un cinèfil aficionat a la informàtica vol crear una Base de Dades que reculli informació diversa sobre el món cinematogràfic, des dels orígens del cinema fins a avui mateix, amb el contingut que es descriu a continuació.
Lògicament, vol tenir classificades moltes pel.lícules, que vindran identificades per un codi. També vol de cadascuna el nom, l’any de l’estrena, el pressupost, el director, etc. A més, de cada pel.lícula vol conéixer també quins actors van intervenir, així com el paper que hi representàven (actor principal, secundari, etc.) i el possible premi que va rebre per la seva interpretació.
Les pel.lícules són d’un tema determinat. Es ben sabut que hi ha actors especialitzats en un tema, encara que un actor és capaç d’interpretar varis temes amb diferent “habilitat”.
Com que el nostre cinèfil és una mica curiós, vol emmagatzemar també dades personals dels actors, que ha anat recollint al llegir revistes del món artístic. Per exemple, quins actors són en certa manera substitutius d’altres, amb un grau de possible substitució que pot anar de 1 a 10. També quins actors són “incompatibles”, o sigui, que mai han treballat ni treballaran junts amb una mateixa pel.lícula o escena.
Els actors estan contractats, en un moment donat per una companyia, però poden canviar si tenen una oferta millor. També poden retornar a una companyia en la que ja hi  havien treballat. Les companyies produeixen pel.lícules, però cap pel.lícula és coproduïda per dues o més companyies.
Com que el nostre amic fa molt de turisme, vol saber, per a cada ciutat, quines companyies hi tenen representació i a quina adreça. Evidentment, les companyies solen tenir representació a quasi totes les ciutats importants. Al mateix temps, vol també informació de quines pel.lícules s’estan rodant a cada ciutat i en quin moment, tenint en compte que una pel.lícula es pot rodar a vàries ciutats i també a una mateixa ciutat en diferents fases del seu rodatge.
Proposar un esquema entitat-relació adient (identificant clarament entitats, atributs i interrelacions) i un esquema lògic amb el seu DR.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[Cinèfil](./cinefil.xml)

## 2.2. Esquema conceptual (EC)
![cinèfil](./cinefil.png)

# 3. Model lògic relacional
## 3.1. Esquema lògic
Tema(<ins>codTema</ins>, descripcioTema)\
Actor(<ins>idActor</ins>, nomActor)\
Especialitzacio(<ins>idActor, codTema</ins>, habilitat)\
Substitucio(<ins>idActor, actorPrincipal</ins>, grau)\
Compatibilitat(<ins>idActor, company</ins>, afinitat)\
Paper(<ins>codPaper</ins>, descripcioPaper)\
Pelicula(<ins>ISBN</ins>, titol, anyEstrena, pressupost, director, idCompanyia, codTema)\
Interpretacio(<ins>ISBN, idActor</ins>, codPaper, premi)\
Fase(<ins>iniciFase</ins>)\
Ciutat(<ins>codCiutat</ins>, ciutat, pais)\
Rodatge(<ins>ISBN, codCiutat, iniciFase</ins>, fiFase)\
Companyia(<ins>idCompanyia</ins>, companyia)\
Seu(<ins>idCompanyia, codCiutat</ins>, adreça)\
Data(<ins>dataInici</ins>)\
Contracte(<ins>idActor, dataInici</ins>, idCompanyia, oferta, dataFi)

## 3.2. Diagrama referencial
Relació referencial|Clau aliena|Relació referida
-|:-:|-
Especialitzacio|idActor|Actor
Especialitzacio|codTema|Tema
Substitucio|idActor|Actor
Substitucio|actorPrincipal|Actor
Compatibilitat|idActor|Actor
Compatibilitat|company|Actor
Pelicula|idCompanyia|Companyia
Pelicula|codTema|Tema
Interpretacio|ISBN|Pelicula
Interpretacio|idActor|Actor
Interpretacio|codPaper|Paper
Rodatge|ISBN|Pelicula
Rodatge|codCiutat|Ciutat
Rodatge|iniciFase|Fase
Seu|idCompanyia|Companyia
Seu|codCiutat|Ciutat
Contracte|idActor|Actor
Contracte|dataInici|Data
Contracte|idCompanyia|Companyia

# 4. Model físic
## 4.1. Enllaç a l'esquema físic
